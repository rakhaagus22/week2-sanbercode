<?php 
    require_once("Animal.php");
    require("Ape.php");
    require("Frog.php");

    $sheep = new Animal("Shaun");
    echo "Nama: " .$sheep->name ."<br>"; // "shaun"
    echo "Jumlah Kaki: " .$sheep->legs ."<br>"; // 4
    echo "Cold Blooded: " .$sheep->cold_blooded ."<br><br>";

    $sungokong = new Ape("Kera Sakti");
    echo "Nama: " .$sungokong->name ."<br>";
    echo "Jumlah Kaki: " .$sungokong->legs ."<br>";
    echo "Cold Blooded: " .$sungokong->cold_blooded."<br>";
    $sungokong->yell(); // "Auooo"

    echo "<br><br>";

    $kodok = new Frog("buduk");
    echo "Nama: " .$kodok->name ."<br>";
    echo "Jumlah Kaki: " .$kodok->legs ."<br>";
    echo "Cold Blooded: " .$kodok->cold_blooded ."<br>";
    $kodok->jump() ; // "hop hop"

?>