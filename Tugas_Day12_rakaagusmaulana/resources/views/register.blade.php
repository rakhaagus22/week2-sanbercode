<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Buat Account Mu!</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sing Up Form</h3>

    <form action="{{ route('register-post') }}" method="POST">
        @csrf
      <label for="firstname">First Name: </label><br />
      <input type="text" name="firstname" id="firstname" /><br />

      <br />

      <label for="lasname">Last Name: </label><br />
      <input type="text" name="lastname" id="lastname" /><br />

      <br />

      <label for="gender">Gender: </label><br /><br />
      <input type="radio" name="gender" id="gender" value="Male" />Male <br />
      <input type="radio" name="gender" id="gender" value="Female" />Female
      <br />
      <input type="radio" name="gender" id="gender" value="Other" />Other <br />

      <br />

      <label for="nationally">Mationally: </label><br /><br />
      <select name="nationally" id="nationally">
        <option value="Indonesia">Indonesia</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Singapura">Singapura</option>
        <option value="India">India</option>
        <option value="Amerika Serikat">Amerika Serikat</option>
        <option value="Afrika">Afrika</option></select
      ><br />

      <br />

      <label for="language">Language Spoken: </label><br /><br />
      <input
        type="checkbox"
        name="language"
        id="language"
        value="Bahasa Indonesia"
      />Bahasa Indonesia <br />
      <input
        type="checkbox"
        name="language"
        id="language"
        value="English"
      />English <br />
      <input type="checkbox" name="language" id="language" value="Other" />Other
      <br />

      <br />

      <label for="Bio">Bio: </label><br /><br />
      <textarea name="Bio" id="Bio" cols="30" rows="10"></textarea><br />
      <input type="submit" name="singup" id="singup" value="Sing Up" />
    </form>
  </body>
</html>
