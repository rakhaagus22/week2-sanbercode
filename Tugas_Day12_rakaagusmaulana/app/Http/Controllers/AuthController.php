<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //

    public function register(){
        return view("register");
    }

    public function post(Request $request){
        // dd($request->all());
        $firstName = $request["firstname"];
        $lastName = $request["lastname"];

        $data = array("firstName" => $firstName, "lastName" => $lastName);

        return view('selamatDatang', $data);
    }
}
