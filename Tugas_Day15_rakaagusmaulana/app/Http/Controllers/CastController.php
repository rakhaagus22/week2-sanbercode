<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = DB::table('cast')->get();

        return view("cast.index", ["cast" => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('cast.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'nama' => 'required|min:2',
            'umur' => 'required',
            'bio' => 'required'
        ], [
            'nama.required' => 'Nama Cast Tidak Boleh Kosong',
            'nama.min' => 'nama Harus Minimal 2 Karakter',
            'umur.required' => 'Umur Cast Tidak Boleh Kosong',
            'bio.required' => 'bio cast Tidak Boleh Kosong'
        ]);

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.show', ['cast' => $cast]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.edit', ['cast' => $cast]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $request->validate([
            'nama' => 'required|min:2',
            'umur' => 'required',
            'bio' => 'required'
        ], [
            'nama.required' => 'Nama Cast Tidak Boleh Kosong',
            'nama.min' => 'nama Harus Minimal 2 Karakter',
            'umur.required' => 'Umur Cast Tidak Boleh Kosong',
            'bio.required' => 'bio cast Tidak Boleh Kosong'
        ]);

        DB::table('cast')
            ->where('id', $id)
            ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio']
                ]
            );

        return redirect('/cast');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        DB::table('cast')->where('id', '=', $id)->delete();

        return redirect('/cast');
    }
}
