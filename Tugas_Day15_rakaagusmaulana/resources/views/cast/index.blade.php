@extends('layout.master')

@section('judul')
Halaman Data Cast
@endsection

@push('script')
<script src="{{('/template/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{('/tamplate/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.12.1/datatables.min.css"/>
@endpush

@section('content')

<a href="{{ route('cast.create') }}" class="btn btn-primary btn-sm my-3">Tambah Data</a>

<table id="example1" class="table table-bordered table-striped">
    <thead>
    <tr>
      <th>#</th>
      <th>Nama</th>
      <th>Umur</th>
      <th>Bio</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
        <tr>
            <td scope="row">{{ $key + 1 }}</td>
            <td>{{ $item->nama }}</td>
            <td>{{ $item->umur }}</td>
            <td>{{ $item->bio }}</td>
            <td>
                <a href="{{ route('cast.show', $item->id) }}" class="btn btn-info btn-sm">Show</a>
                <a href="{{ route('cast.edit', $item->id) }}" class="btn btn-warning btn-sm">Edit</a>
                <form action="{{ route('cast.destroy', $item->id) }}" method="POST">
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
            </td>
        </tr>
        @empty
            <h1>Data Kosong</h1>
        @endforelse
    </tbody>
</table>
@endsection


