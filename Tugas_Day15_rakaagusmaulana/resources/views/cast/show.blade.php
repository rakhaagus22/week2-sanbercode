@extends('layout.master')

@section('judul')
Halaman Show Cast
@endsection

@section('content')

<h1 class="text-primary">{{ $cast->nama }}</h1>
<h4>{{ $cast->umur }}</h4>
<p>{{ $cast->bio }}</p>

<a href="{{ route('cast.index') }}" class="btn btn-primary btn-sm my-3">Kembali</a>
@endsection


