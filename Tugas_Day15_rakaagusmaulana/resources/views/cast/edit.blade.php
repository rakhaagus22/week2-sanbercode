@extends('layout.master')

@section('judul')
Halaman Edit Data Cast
@endsection

@section('content')
<div>
        <form action="{{ route('cast.update', $cast->id) }}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Nama Cast</label>
                <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan Nama Cast" value="{{ old("nama", $cast->nama) }}">
                @error('nama')
                    <div class="alert alert-danger my-3">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Umur Cast</label>
                <input type="number" class="form-control" name="umur" id="body" placeholder="Masukkan Umur Cast" value="{{ old("umur", $cast->umur) }}">
                @error('umur')
                    <div class="alert alert-danger my-3">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Bio Cast</label>
                <textarea name="bio" id="" cols="30" class="form-control" rows="10">{{ old("bio", $cast->bio) }}</textarea>
                @error('bio')
                    <div class="alert alert-danger my-3">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection


